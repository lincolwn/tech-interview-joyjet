
# Joyjet Tech Interview

This is a tech test made to Joyjet Digital Space Agency. Below we have instrunction how to deploy and test it.

# Installation

## Clone repository

First of all, you need to clone this repository to your local machine. To do that, create a folder and after clone repository:

```bash
mkdir joyjet-test && cd joyjet-test
git clone https://<your-username>@bitbucket.org/lincolwn/tech-interview-joyjet.git project
```

## Virtual Environment

In Python projects, is interesting create a virtual environments to isolate you project to the global python.

```bash
virtalenv venv -p python3
```

Activate Virtual Environment:

```bash
# on mac
. venv/bin/activate

#on linux
source venv/bin/activate
```
## Requirements and Tests

In this app, we don't have dependencies to production, but to test environment we have. If you have test, you'll need to install packages defined on file `requiriments.txt`:

```bash
cd project/src
pip install -r requirements.txt
```

Now, you can execute tests with the command:

```bash
py.test
```
Here, we use `pytest` framework to test instead `unittest`.

## Executing Application

Now, that everything is ready, we can execute the app. Execute the command below and the application will be start.

```bash
python main.py
```
## About Code

### Structure

The application is in `src` package. There we have a `main.py` module that is the entry point to the app an another package named `app`.
Inside `app` package we have common python modules to all application levels, and three packages: `level1`, `level2`, `level3`.
These last packages contain specific code to each level, a `settings` module to respective environment.
When we execute the application, each level is executed sequentially. First `level1`, after `level2` and after `level3`, and a file named `output.json` is generated and saved in respective folders. This file is result of each leval based on file `data.json` how to required for test.

### Patterns

Here, I used **DDD (Domain Driven Design)** approach, and we can see patterns envolved in this approach:

- **Repository Pattern**: Used to abstract Data Access Layer to the rest of the application.
- **Domain Model**: Model classes that represents the business entities, without external dependencies.
- **Value Object**: Like Model classes, but is used just to transform simple data in python object, (eg.: Address)
- **Service**: Service should contain all business rules that don't belongs to any domain model objects. (eg. delivary fee)

Beyond it, I create a module named `dataset.py`. This module contain a base class to access data, and one implementation that is used to access data from json files.

## Finals Considerations

This is all considerantions that I have to do about this project. If you have any questions to do about it, you may contact to me.

Thank you for your attention :)
