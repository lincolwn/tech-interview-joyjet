import os

if __name__ == '__main__':
    os.environ.setdefault('SETTINGS_MODULE', 'app.level1.settings')
    from app.level1.services import service_exec_level1

    message = (
        '''
        Olá. Seja bem-vindo!
        Esta é uma aplicação de teste para JoyJet Digital Space Agency
        
        Deseja continuar e gerar os dados de saida com base nos
        arquivos 'data.json'?
        '''
    )
    print(message)
    key = input('[y]/n: ')

    if key == 'y' or 0x13:

        print('\nexecutando level 1...')
        service_exec_level1()
        print('level 1 executado com sucesso!')
        
        os.environ['SETTINGS_MODULE'] = 'app.level2.settings'
        from app.level2.services import service_exec_level2
        print('\nexecutando level 2...')
        service_exec_level2()
        print('level 2 executado com sucesso!')

        os.environ['SETTINGS_MODULE'] = 'app.level3.settings'
        from app.level3.services import service_exec_level3
        print('\nexecutando level 3...')
        service_exec_level3()
        print('level 3 executado com sucesso!')
        
    print('\n\nObrigado :)')
    message = (
        '''
        
        Aplicação desenvolvida por Lincolwn de Lima Martins.
        '''
    )
    print(message)