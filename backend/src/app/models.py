from functools import reduce
from .repository import Repository
import math

class Article:
    
    class Meta:
        collection_name = 'articles'
    
    def __init__(self, id, name, price):
        self.id = id
        self.name = name
        self.price = price


class Cart:
    
    class Meta:
        collection_name = 'carts'

    def __init__(self, id, items):
        self.id = id
        self.items = Item.create_many(items) if len(items) > 0 else []
        

    def sum_items_price(self):
        if len(self.items) > 0:
            new_items = list(map(lambda x: x.article.price*x.quantity, self.items))
            return reduce(lambda x,y: x+y, new_items)
        return 0


class Item:
    
    class Meta:
        collection_name = 'items'

    def __init__(self, article_id, quantity):
        self.article_id = article_id
        self.quantity = quantity
        self._article_repository = Repository(Article)

    @classmethod
    def create_many(cls, data):
        if isinstance(data, (list, tuple)):
            collection = []
            for item in data:
                collection.append(Item(**item))
            return collection
        return None

    @property
    def article(self):
        return self._article_repository.get_by_id(self.article_id)


class TransactionVolume:
    '''
    Value Object that representing the min and max value from
    transaction [reference DDD (Domain Driven Design)]
    '''
    def __init__(self, min_price, max_price):
        self.min_price = min_price
        self.max_price = max_price


class DeliveryFee:
    '''
    To see reference about value object, see DDD (Domain Driven Design)
    '''
    class Meta:
        collection_name = 'delivery_fees'

    def __init__(self, eligible_transaction_volume, price):
        self.limits = TransactionVolume(**eligible_transaction_volume)
        self.price = price


class Discount:
    
    AMOUNT = 'amount'
    PERCENTAGE = 'percentage'

    class Meta:
        collection_name = 'discounts'

    def __init__(self, article_id, type, value):
        self.article_id = article_id
        self.type = type
        self.value = value
        self._article_repository = Repository(Article)

    @property
    def article(self):
        return self._article_repository.get_by_id(self.article_id)

    def calculate_discount(self, item):
        '''
        return new value to 1 item with discount if it applicable,
        0 otherwise
        '''
        if item.article_id == self.article_id:
            if self.type == self.AMOUNT:
                return self.value
            elif self.type == self.PERCENTAGE:
                return item.article.price*(self.value/100)
        return 0