import os
import importlib

class Settings:
    '''
    Proxy para acessar o arquivo de configuração do ambiente correto
    (level1, level2, level3)
    '''
    def __getattr__(self, name):
        settings_module = os.environ.get('SETTINGS_MODULE')
        if settings_module:
            settings = importlib.import_module(settings_module)
        return getattr(settings, name)

settings = Settings()