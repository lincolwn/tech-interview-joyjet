import json
import os


SETTINGS_VAR = 'SETTINGS_MODULE'

def test_level1(mocker):
    os.environ[SETTINGS_VAR] = 'src.app.level1.settings'
    mocker.patch('src.app.utils.export_to_json')
    from .level1.services import service_exec_level1
    from .settings import settings

    with open(settings.OUTPUT_PATH_TEST, 'r') as file:
        output = json.load(file)
    result = service_exec_level1()

    assert len(result['carts']) == len(output['carts'])
    for i in range(0, len(result['carts'])):
        assert result['carts'][i]['total'] == output['carts'][i]['total']


def test_level2(mocker):
    os.environ[SETTINGS_VAR] = 'src.app.level2.settings'
    mocker.patch('src.app.utils.export_to_json')
    from .level2.services import service_exec_level2
    from .settings import settings

    with open(settings.OUTPUT_PATH_TEST, 'r') as file:
        output = json.load(file)
    result = service_exec_level2()

    assert len(result['carts']) == len(output['carts'])
    for i in range(0, len(result['carts'])):
        assert result['carts'][i]['total'] == output['carts'][i]['total']


def test_level3(mocker):
    os.environ[SETTINGS_VAR] = 'src.app.level3.settings'
    mocker.patch('src.app.utils.export_to_json')
    from .level3.services import service_exec_level3
    from .settings import settings

    with open(settings.OUTPUT_PATH_TEST, 'r') as file:
        output = json.load(file)
    result = service_exec_level3()

    assert len(result['carts']) == len(output['carts'])
    for i in range(0, len(result['carts'])):
        assert result['carts'][i]['total'] == output['carts'][i]['total']