class ObjectsDoesNotExist(Exception):
    '''
    Indicates that an object collection does not exists at dataset.
    '''