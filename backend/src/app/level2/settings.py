import os

DATA_PATH = f'{os.getcwd()}/../level2/data.json'
OUTPUT_PATH = f'{os.getcwd()}/app/level2/output.json'
OUTPUT_PATH_TEST = f'{os.getcwd()}/../level2/output.json'