from ..models import Cart, Item, DeliveryFee
from ..repository import Repository
from ..utils import export_to_json
import json

def service_exec_level2():
    _carts = Repository(Cart).fetch_all()
    carts = []
    for cart in _carts:
        total = cart.sum_items_price()
        total += service_calculate_delivery_fee(total)
        carts.append({'id': cart.id, 'total': total})
    export_to_json({'carts': carts})
    return {'carts': carts}


def service_calculate_delivery_fee(value_transaction):
    repository = Repository(DeliveryFee)
    _fees = repository.fetch_all()
    fee_value = 0
    for fee in _fees:
        if fee.limits.max_price:
            if fee.limits.min_price <= value_transaction < fee.limits.max_price:
                fee_value = fee.price
                break
        elif value_transaction >= fee.limits.min_price:
            fee_value = fee.price
            break
                
    return fee_value