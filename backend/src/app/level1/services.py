from ..models import Cart, Item
from ..repository import Repository
from ..utils import export_to_json
import json

def service_exec_level1():
    _carts = Repository(Cart).fetch_all()
    carts = []
    for cart in _carts:
        carts.append({'id': cart.id, 'total': cart.sum_items_price()})
    export_to_json({'carts': carts})
    return {'carts': carts}