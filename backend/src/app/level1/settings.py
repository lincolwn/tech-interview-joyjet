import os

DATA_PATH = f'{os.getcwd()}/../level1/data.json'
OUTPUT_PATH = f'{os.getcwd()}/app/level1/output.json'
OUTPUT_PATH_TEST = f'{os.getcwd()}/../level1/output.json'