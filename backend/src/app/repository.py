from .dataset import JsonDataAccess
from functools import lru_cache
import os

class Repository:

    def __init__(self, model):
        self.dataset = JsonDataAccess(model=model)
    
    def fetch_all(self):
        return self.dataset.fetch()

    def get_by_id(self, id):
        try:
            l = list(filter(lambda x: x.id==id, self.fetch_all()))
            return l[0]
        except IndexError:
            return None