from .settings import settings
import json

def export_to_json(payload):
    with open(settings.OUTPUT_PATH, 'w', encoding='utf-8') as file:
        json.dump(payload, file, ensure_ascii=False, indent=4)
    print(f"arquivo exportado para: {settings.OUTPUT_PATH}")