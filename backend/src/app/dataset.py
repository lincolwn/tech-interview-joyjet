import os
import json
from .exceptions import ObjectsDoesNotExist
from .settings import settings

class DataAccessObject:
    '''
    Base class to access data
    '''

    def __init__(self, model):
        self.model = model

    def fetch(self, **kwargs):
        NotImplemented('Must be implemented by child classes.')

    def save(self, instance):
        NotImplemented('Must be implemented by child classes.')


class JsonDataAccess(DataAccessObject):
    '''
    Interface to persiste data to json files.
    '''
        
    def __init__(self, model):
        super().__init__(model)
        self.input_file_path = settings.DATA_PATH
        self.output_file_path = settings.OUTPUT_PATH


    def fetch(self, **kwargs):
        self.dataset = None
        collection_name = self.model.Meta.collection_name
        with open(self.input_file_path, 'rb') as file:
            data = json.load(file)
        self.dataset = data.get(collection_name)
        if not self.dataset:
            raise ObjectsDoesNotExist(f"'{collection_name}' does not exists at data.json")
        objects = []
        for item in self.dataset:
            objects.append(self.model(**item))
        return objects
