import os

DATA_PATH = f'{os.getcwd()}/../level3/data.json'
OUTPUT_PATH = f'{os.getcwd()}/app/level3/output.json'
OUTPUT_PATH_TEST = f'{os.getcwd()}/../level3/output.json'