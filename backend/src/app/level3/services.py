from ..models import Cart, Item, DeliveryFee, Discount
from ..repository import Repository
from ..utils import export_to_json
from ..level2.services import service_calculate_delivery_fee
import json
import math

def service_exec_level3():
    repository = Repository(Cart)
    _carts = repository.fetch_all()
    carts = []
    for cart in _carts:
        total = cart.sum_items_price()
        total -= service_calculate_discount(cart)
        total += service_calculate_delivery_fee(total)
        total = int(math.modf(total)[1])
        carts.append({'id': cart.id, 'total': total})  
    export_to_json({'carts': carts})
    return {'carts': carts}


def service_calculate_discount(cart):
    _discounts = Repository(Discount).fetch_all()
    total_discount = 0
    articles = list(map(lambda discount: discount.article_id, _discounts))
    for item in cart.items:
        if item.article_id in articles:
            discount = _discounts[articles.index(item.article_id)]
            total_discount += discount.calculate_discount(item) * item.quantity
    return total_discount